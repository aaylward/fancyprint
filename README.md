# fancyprint
Fancy version of `print()` that can add color and style

## Installation
```sh
pip3 install fancyprint
```
or
```sh
pip3 install --user fancyprint
```

## Example

Print red text:
```python
from fancyprint import fprint
fprint('hello world', color='r')
```